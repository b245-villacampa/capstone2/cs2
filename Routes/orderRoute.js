const express= require("express");
const router= express.Router();
const orderController = require("../Controllers/orderController.js");
const auth = require("../auth.js");

router.post("/createOrder", auth.verify, orderController.createOrder);
router.get("/allOrders", auth.verify, orderController.allOrders);
router.get("/:userId", auth.verify, orderController.getUserDetails);

module.exports = router;