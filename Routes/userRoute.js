const express = require("express");
const router = express.Router();
const userController = require("../Controllers/userController.js");
const orderController = require("../Controllers/orderController.js");
const auth = require("../auth.js");

router.post("/register", userController.registration);
router.post("/login", userController.login);
router.get("/getAllOrders", auth.verify, userController.getAllOrders)

// Router with params
router.get("/profile", auth.verify, userController.profile);
router.put("/:userId", auth.verify, userController.setAsAdmin);
	 
module.exports = router;