const express = require('express');
const router = express.Router();
const cartController = require('../Controllers/cartController.js');
const auth = require('../auth.js');

router.post('/checkout', auth.verify, cartController.checkout);

router.put('/:cartId', auth.verify, cartController.inActiveStatus);
router.post('/:productId', auth.verify, cartController.addToCart);
router.get('/:userId', auth.verify, cartController.viewAllItems);
router.put('/:cartId', auth.verify, cartController.updateQuantity);
router.get('/:userId/:productId', auth.verify, cartController.singleItem);



module.exports = router;