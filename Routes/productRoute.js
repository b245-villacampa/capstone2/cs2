const express = require("express");
const router = express.Router();
const productController = require("../Controllers/productController.js");
const auth = require("../auth.js");

router.post("/inputProduct", auth.verify, productController.inputProduct);
router.get("/active", productController.getActiveProducts);

router.get("/all", productController.getAllProducts);
router.get("/:id", productController.singleProduct);
router.put("/:id/update", auth.verify, productController.updateProductInfo);
router.put("/:productId/archive", auth.verify, productController.archiveProduct);
router.put("/:productId/restore", auth.verify, productController.restoreProduct);

module.exports = router;