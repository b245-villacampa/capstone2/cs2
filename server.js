const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./Routes/userRoute.js");
const productRoute = require("./Routes/productRoute.js");
const orderRoute = require("./Routes/orderRoute.js")
const cartRoute = require("./Routes/cartRoute.js");

const port = 4000;
const app = express();

	//[mongoDB Connection]
	mongoose.connect("mongodb+srv://admin:admin@batch245-villacampa.g4r4ic2.mongodb.net/Capstone2?retryWrites=true&w=majority", {
		useNewUrlParser:true,
		useUnifiedTopology:true
	})

	let db = mongoose.connection;

	// For error handling
	db.on("error", console.error.bind("Connection Error!"));

	// For validation 
	db.once("open", ()=>{console.log(`We are connected to the cloud!`)})



// middleWares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

// Routing
app.use("/user", userRoute);
app.use("/product", productRoute);
app.use("/order", orderRoute);
app.use("/cart", cartRoute);

app.listen(port, ()=> console.log(`Server is running at ${port}`))