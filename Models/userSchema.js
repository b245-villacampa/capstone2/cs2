const mongoose = require("mongoose");

const usersSchema = new mongoose.Schema({
	firstName:{
		type:String,
		required:[true, "First name is required!"]
	},
	lastName:{
		type:String,
		required:[true, "Last name is required!"]
	},
	addressLine:{
		type:String,
		default:" "
	},
	city:{
		type:String,
		default:" "
	},
	province:{
		type:String,
		default:" "
	},
	zipCode:{
		type:Number,
		default:" "
	},
	country:{
		type:String,
		default:" "
	},
	email:{
		type:String,
		required:[true, "Email is required!"]
	},
	password:{
		type:String,
		required:[true, "Password is required!"]
	},
	mobileNo:{
		type:String,
		default:" "
	},
	isAdmin:{
		type:Boolean,
		default:false
	},
	createdOn:{
		type:Date,
		default: new Date()
	},
	updatedOn:{
		type:Date,
		default: new Date()
	}
	
});


module.exports = mongoose.model("User", usersSchema);
