const mongoose = require("mongoose");
const productsSchema = new mongoose.Schema({
	addedBy:{
		type:String,
		required:[true, "addedBy is required!"]
	},
	updatedBy:{
		type:String,
		default:" "
	},
	productPhoto:{
		type:String,
		required:[true, "Product1 Photo is required!"]
	},
	category:{
		type:String,
		required:[true, "Category of the product is required!"]
	},
	name:{
		type:String,
		required:[true, "Name of the product is required!"]
	},
	description:{
		type:String,
		required:[true, "Description of the product is required!"]
	},
	brand:{
		type:String,
		required:[true, "Brand of the product is required!"]
	},
	model:{
		type:String,
		required:[true, "Model of the product is required!"]
	},
	regPrice:{
		type:Number,
		required:[true, "Price of the product is required!"]
	},
	stocks:{
		type:Number,
		required:[true, "Price of the product is required!"]
	},
	isActive:{
		type:Boolean,
		default:true
	},
	createdOn:{
		type:Date,
		default:new Date
	},
	updatedOn:{
		type:Date,
		default:new Date
	}
});

module.exports = mongoose.model("Product", productsSchema)