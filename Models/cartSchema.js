const mongoose = require('mongoose');
const cartSchema = mongoose.Schema({

	userId:{
		type:String,
		required:[true, "User Id is required"]
	},
	productId:{
		type:String,
		required:[true, "Product Id is required!"]
	},
	quantity:{
		type:Number,
		required:[true, "Quantity is required!"]
	},
	subTotal:{
		type:Number,
		required:[true, "subTotal is required!"]
	},
	addedToCartOn:{
		type:Date,
		default:new Date()
	},
	isActive:{
		type:Boolean,
		default:true
	},
	updatedOn:{
		type:Date,
		default:new Date()
	}
	
}) 

module.exports = mongoose.model("Cart", cartSchema)