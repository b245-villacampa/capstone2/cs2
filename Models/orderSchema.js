const mongoose = require("mongoose");
const orderSchema = mongoose.Schema({
	userId:{
		type:String,
		required:[true, "User Id is required!"]
	},
	
	productId:{
		type:String,
		required:[true, "User Id is required!"]
	},
	quantity:{
		type:Number,
		required:[true, "Quantity is required!"]
	},
	totalAmount:{
		type:Number,
		required:[true, "totalAmount is required!"]
	},
	purchasedOn:{
		type:Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Order", orderSchema);