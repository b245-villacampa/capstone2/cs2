const mongoose = require('mongoose');
const Product = require('../Models/productSchema.js');
const User = require('../Models/userSchema.js');
const Order = require('../Models/orderSchema.js');
const Cart = require('../Models/cartSchema.js');
const auth = require('../auth.js');

// Adding to cart
module.exports.addToCart = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;
	const input = request.body;
	let regPrice;

	if(!userData.isAdmin){
		Product.findById(productId).then(result=>{

			if(result!==null){
				
					let added = new Cart({
						userId:userData._id,
						productId:productId,
						quantity:input.quantity,
						subTotal:input.quantity*result.regPrice,
						addedToCartOn:new Date
					})

					added.save().then(save=>response.send(save)).catch(error=>response.send(error))
			}else{
				return response.send("Product not exist!")
			}
			
		}).catch(error=> response.send(error))
	}else{
		return response.send("You're an admin!")
	}
}

// Retrieve all the items in the cart of a specific user

module.exports.viewAllItems = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const userId = request.params.userId;

	Cart.find({userId:userId}).then(result => response.send(result)).catch(error => response.send(error))

}

module.exports.singleItem = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const userId = request.params.userId;
	const productId = request.params.productId;


	Cart.find({userId:userId}).then(result => {
		if(result.productId === productId && userId === result.userId){
			response.send(result)
		}else{
			response.send(false)
		}
		

	}).catch(error => response.send(error))

}

// Update the quantity

module.exports.updateQuantity = (request, response) =>{
	
	const userData = auth.decode(request.headers.authorization);
	const cartId = request.params.cartId;
	const input = request.body;

	if(!userData.isAdmin){
		Cart.findById(cartId).then(result=>{

			if(result !== null){
				Product.findById(result.productId).then(result=>{

					let update = {

						quantity:input.quantity,
						subTotal:input.quantity*result.regPrice,
						updatedOn:new Date
					}

					Cart.findByIdAndUpdate(cartId, update, {new:true}).then(update=>response.send(update)).catch(error=>response.send("Error in update"))
				}).catch(error=>response.send("Error in the product!"))
			
			}else{
				return response.send("Cart does not exist!");
			}
		})
	}
	else{
		return response.send("You dont have access!");
	}
}

// Update the status of the item

module.exports.inActiveStatus = (request, response) =>{

	const userData = auth.decode(request.headers.authorization);
	const cartId = request.params.cartId;

	if(!userData.isAdmin){

		Cart.findByIdAndUpdate(cartId, {isActive:false, updatedOn:new Date}, {new:true}).then(update=>response.send(update)).catch(error=>response.send(error))
	}
}

// Checkout

// Checkout 

module.exports.checkout = (request, response)=>{
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;

	if(!userData.isAdmin){
		let isCheckout = Cart.findById(input.cartId).then(result=>{
			if(result===null){
				return false
			}else{
				let purchase = new Order({
					userId:userData._id,
					productId:result.productId,
					quantity:result.quantity,
					totalAmount:result.subTotal
				})
				purchase.save().then(update=>response.send(update)).catch(error=>response.send(error))
			}
		
		})
		if(isCheckout){
			Cart.findByIdAndUpdate(input.cartId, {isActive:false, updatedOn:new Date}, {new:true}).then(update=>response.send(update)).catch(error=>response.send(error))
		}

	}else{
		return response.send("You're an admin!")
	}
}