const mongoose = require("mongoose");
const Product = require("../Models/productSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// Create product input (admin only)
module.exports.inputProduct = (request, response) =>{
	const input = request.body;
	const userData = auth.decode(request.headers.authorization);


	if(!userData.isAdmin){
		return response.send("You don't have access to input a product! You're not an admin");
	}else{
		let newProduct = new Product({
			addedBy:userData._id,
			name: input.name,
			description: input.description,
			brand: input.brand,
			model: input.model,
			regPrice:input.regPrice,
			productPhoto:input.productPhoto,
			category:input.category,
			stocks:input.stocks
		});
		
		return newProduct.save().then(save=> response.send(save)).catch(error=>response.send(error));
	}
}


// Retrieve all products
module.exports.getAllProducts = (request, response)=>{
		Product.find().then(result=> response.send(result)).catch(error=>response.send(error))
	
}
// Retrieve active products
module.exports.getActiveProducts = (request, response)=>{
		Product.find({isActive:true}).then(result=> response.send(result)).catch(error=>response.send(error))
	
}


// Retrieve single product 
module.exports.singleProduct = (request, response)=>{
	const id = request.params.id;

	Product.findById(id).then(result => response.send(result)).catch(error => response.send(error));
}

// Update Product Info
module.exports.updateProductInfo=(request, response)=>{

	const userData = auth.decode(request.headers.authorization);
	const id = request.params.id;
	const input = request.body;

	if(!userData.isAdmin){
		return response.send(false);
	}else{
		Product.findById(id).then(result=>{
			if(result === null){
				return response.send(false)
			}else{
				let updateProduct = {
					updatedBy:userData._id,
					name: input.name,
					description: input.description,
					brand: input.brand,
					model: input.model,
					regPrice:input.regPrice,
					stocks:input.stocks,
					updatedOn:new Date
				}

				Product.findByIdAndUpdate(id, updateProduct, {new:true}).then(update => { console.log(update.updatedOn); 
					return response.send(update)}).catch(error=>response.send(error))
			}
		}).catch(error=>response.send(error))
	}
}

// Archive Product
module.exports.archiveProduct = (request, response)=>{
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	if(!userData.isAdmin){
		return response.send(false);
	}else{
		Product.findByIdAndUpdate(productId, {isActive:false, updatedOn:new Date, updatedBy:userData._id}, {new:true}).then(result=>{
				if(result === null){
					return response.send(false)
				}else{
					return response.send(result);
				}
		}).catch(error=>response.send(error))
	}
}

module.exports.restoreProduct = (request, response)=>{
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	if(!userData.isAdmin){
		return response.send(false);
	}else{
		Product.findByIdAndUpdate(productId, {isActive:true, updatedOn:new Date, updatedBy:userData._id}, {new:true}).then(result=>{
				if(result === null){
					return response.send(false)
				}else{
					return response.send(result);
				}
		}).catch(error=>response.send(error))
	}
}
