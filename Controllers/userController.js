const mongoose = require("mongoose");
const User = require("../Models/userSchema.js");
const Order = require("../Models/orderSchema.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt");

// Registration
module.exports.registration = (request, response) =>{

	const input = request.body;

	User.findOne({email:input.email}).then(result =>{
		if(result != null){
			return response.send(false);
		}else{
			let newUser = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10)
				
			})
			newUser.save().then(save=> {return response.send(true)}).catch(error=>{return response.send(false)});
		}
	}).catch(error=> response.send(false))
}

//Log In controller   
module.exports.login = (request, response)=>{
	const input = request.body;

	User.findOne({email:input.email}).then(result=>{
		
		if(result !== null){
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password);
			if(isPasswordCorrect){
				return response.send({auth: auth.createToken(result)})
			}else{
				return response.send(false)
			}
		}else{
			return response.send(false)
		}
	}).catch(error=> response.send(false))
}

// Get Profile to check(admin only)
module.exports.profile = (request, response)=>{
	const userData = auth.decode(request.headers.authorization);

	User.findById(userData._id).then(result=>{
		if(result!=null){
			result.password=" "
			return response.send(result)
		}else{
			return response.send(false)
		}
	}).catch(error => response.send(false))

}

// Stretch Goals

// Set user as admin
module.exports.setAsAdmin = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);
	const userId = request.params.userId;

	if(!userData.isAdmin){
		return response.send("You don't have an access to set user to admin!")
	}else{
		User.findByIdAndUpdate(userId, {isAdmin:true}, {new:true}).then(save=> {
			save.password=" ";
			response.send(save)
		}).catch(error=>response.send(error));
	}
}

// Get all user's orders
module.exports.getAllOrders =(request, response) =>{
	const userData = auth.decode(request.headers.authorization);
	if(!userData.Admin){
		Order.findOne({userId:userData._id}).then(result=> {
			if(result===null){
				return response.send("You don't have any orders yet!")
			}else{
				return response.send(result)
			}
		}).catch(error=>response.send(error));
	}else{
		return response.send(false)
	}
	
}