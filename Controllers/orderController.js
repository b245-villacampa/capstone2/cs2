const mongoose = require("mongoose");
const Product = require("../Models/productSchema.js");
const User = require("../Models/userSchema.js");
const Order = require("../Models/orderSchema.js");
const auth = require("../auth.js");

//Creating an order
module.exports.createOrder = (request, response)=>{
	const userData = auth.decode(request.headers.authorization);
	const input = request.body;
	

	if(userData.isAdmin){
		return response.send(true)
	}else{
		User.findById(userData._id).then(result=>{

			if(result === null){
				return response.send(true)
			}else{
				Product.findById(input.productId).then(result =>{
					if(result != null){
						let userOrder = new Order({
							userId:userData._id,
							productId:input.productId,
							quantity:input.quantity,
							totalAmount:input.quantity*result.regPrice
						})
						userOrder.save().then(save=> response.send(save)).catch(error=>response.send("Error on saving!"));


					}else{
						return response.send(false)
					}

				}).catch(error=>response.send(false));

			}
		}).catch(error=>response.send(false));

		
	}
	
}

// Retrieving user details
module.exports.getUserDetails =(request, response)=>{
	const userData = auth.decode(request.headers.authorization);
	const userId = request.params.userId;

	if(!userData.isAdmin){
		return response.send("You don't have access!")
	}else{
		User.findById(userId).then(result=> response.send(result)).catch(error=>response.send(error));
	}

}

// Stretch Goals

//Retrieve all orders(admin only) 
module.exports.allOrders=(request, response)=>{
	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send("You are not an admin!");
	}else{
		Order.find({}).then(result=>response.send(result)).catch(error=>response.send(error));
	}
}