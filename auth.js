const jwt = require("jsonwebtoken");
const secretMsg = "capstone2";

module.exports.createToken = (user) =>{
	const data = {
		_id:user._id,
		email:user.email,
		isAdmin:user.isAdmin
	}

	return jwt.sign(data, secretMsg, {})
}

module.exports.verify = (request,response,next)=>{
	let token = request.headers.authorization;

	if(typeof token === "undefined"){
		return response.send({auth:"Error"})
	}else{
		token = token.slice(7, token.length);
			
			return jwt.verify(token, secretMsg, (error, data)=>{
				if(error){
					return response.send({auth:"Failed!"})
				}else{
					next();
				}
			})
		
	}
}

module.exports.decode = (token) =>{
	if(typeof token === "undefined"){
		return response.send("Error! Token is undefined");
	}else{
		token = token.slice(7, token.length);
		return jwt.verify(token, secretMsg, (error, data)=>{
			if(error){
				return response.send("Error! Token is not verified!")
			}else{
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	}
}